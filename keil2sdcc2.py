import string
import re
import sys

'''
keil2sdcc2.py -

    Converts 8051 register values from Keil to SDCC format.

TODO
- Test cli filename input
- Substitute bracketed macros with actual address (e.g. (PSW^7) as SDCC treats
these as initialisations rather than macro defs. Painful.

'''

# Get command line arguments
'''
arg1 = sys.argv[1]
if arg1 == None:
    file_in = "input.h"  
arg2 = sys.argv[2]
if arg2 == None:
    file_out = "output.h"
'''
file_in = "input.h"
file_out = "output.h"

fi = open(file_in,"r")
fo = open(file_out,"w+")

for line in fi:
    # Get line after line
    l = line
   
    # Find sfr & sbit references
    s1 = 'sfr|sbit'   
    S = re.compile(s1)
    res = S.match(l)
    if res:
        r1 = l.split(' ')
        print(r1)
        # Find length of list r1
        slen = len(r1)
        # Find hex address with ;\n attached
        hex1 = r1[slen-1];
        hex2 = hex1.replace(";\n","");
        # Start building new string
        r2 = r1[1].replace("=","")
        newString = "__" + r1[0] + " __at (" + hex2 + ") " + r2 + ";\n";
        fo.write(newString)
   
    # If no sbit or sfr, just write out
    if res == None:
        fo.write(line)

fo.close()
